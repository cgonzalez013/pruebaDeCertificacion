class Product < ActiveRecord::Base
	has_many :orders
	has_many :bills, through: :orders

	validates_presence_of :price, :name
	validates :price, :numericality => { :greater_than_or_equal_to => 0 }
end
