# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

 Product.create([{name: "Berenjena", price: 5}, {name: "Onion", price: 6}])
 Bill.create({orders_attributes: [{product_id: 1, quantity: 2}, {product_id: 2, quantity: 1}]})