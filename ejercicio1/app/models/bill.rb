class Bill < ActiveRecord::Base
	has_many :orders
	has_many :products, through: :orders

	accepts_nested_attributes_for :orders

	after_create :set_total

	def set_total
		self.total= 0
		Order.where(bill_id: self.id).each do |order|
			self.total+= Product.find(order.product_id).price* order.quantity
		end
		self.save
	end
end
